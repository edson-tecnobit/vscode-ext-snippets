# VSCode Snippets
## Tecnobit ExtJS code snippets for Visual Code

## Summary

- [Configuration](#configuration)

### Configuration
Create your own snippets for VSCode

Open Visual Studio Code and then go to **File > Preferences > User Snippets** and select **"Javascript"**
If you don’t have any snippets yet and you just have comments, you can delete the contents 
and paste all the contents from [**javascript.json**](https://gitlab.com/edson-tecnobit/vscode-ext-snippets/blob/master/javascript.json).

Once saved, open a new file called test.js to see if works. 
On line 1 begin typing **"edit"** and you will notice your new custom snippet selected in the popup.
![Test01](https://gitlab.com/edson-tecnobit/vscode-ext-snippets/raw/128b2d34e93bde17fc0adb93a3c87a069de346a9/snippets.gif)